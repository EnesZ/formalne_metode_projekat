from functools import reduce

ALFABET_DFA = ['0','1']
ALFABET_NFA = ALFABET_DFA.copy()
ALFABET_NFA.append('e')

# dfa cvor
class DFA_Stanje:
    def __init__(self,index,grane,zavrsno,dostupno):
        self.index = index
        self.grane = {}
        #npr grane = {'0':None,'1':0}
        for a in ALFABET_DFA:
            self.grane[a] = grane[a]
        self.zavrsno = zavrsno
        # ako krenemo od pocetnog stanja da li mozemo doci do ovog stanja
        #samo potrebno prilikom konvertovanja iz NFA
        self.dostupno = dostupno

class DFA:
    def __init__(self,broj_stanja):
        self.broj_stanja = 0
        self.stanja = []
        for i in range(broj_stanja):
            self.dodaj_stanje()
        self.pocetno_stanje = self.stanja[0]

    #kreira DFA cvor cije grane ne idu nigdje
    def dodaj_stanje(self):
        grane = {}
        # grane = {'0': None,'1': None}
        for a in ALFABET_DFA:
            grane[a] = None
        #DFA_Stanje(index,grane,zavrsno,dostupno)
        self.stanja.append(DFA_Stanje(self.broj_stanja,grane,False,False))
        self.broj_stanja+=1

    def povezi_stanja(self):
        #samo za provjeru da li je ispravno unesena vrijednost
        stanja_DFA = []
        for i in range(self.broj_stanja):
            stanja_DFA.append(i)

        print("Unos DFA")
        for q in self.stanja:
            for a in ALFABET_DFA:
                while True:
                    stanje = input("Trenutno stanje je q"+str(q.index)+" citamo znak "+a+" i idemo u stanje")
                    stanje = int(stanje)

                    if stanje in stanja_DFA:
                        q.grane[a] = stanje
                        break
            i = input("Da li je ovo stanje zavrsno ? 1 - Da, bilo sta drugo - Ne ")
            if i == '1':
                q.zavrsno = True
                #for s in stanja:
                #    self.stanja[int(s)].dostupno = True

    def ispisi(self):
        print("Ispis DFA")
        print("Pocetno stanje je q" + str(self.pocetno_stanje.index))
        for s in self.stanja:
            print("Iz stanja q"+str(s.index)+" imamo")
            for a in ALFABET_DFA:
                print("granu",a,"koja ide u q"+str(s.grane[a]))
            print("Stanje q"+str(s.index)+(" je " if s.zavrsno else " nije ")+"zavrsno")
    def obrisi_nepotrebna_stanja(self):
        # uklanjanje nepotrebnih stanja
        # rekurzivno cemo ici po granama, DFS rekurzivni
        # kada dodjemo do stanja postavljamo da je dostupno = True
        pregledana_stanja = []
        stanje = self.pocetno_stanje
        self.provjera_dostupnosti(stanje, pregledana_stanja)
        # ako imamo stanja [q0,q1,q2,q3] i ako izbacimo q2 tada stanje q3 postaje q2
        # sto znaci da sve grane koje idu u q3 treba preimenovati da idu q2
        nova_lista_stanja = []
        broj_ukljonjenih_stanja = 0
        # {stari_index:novi_index}
        # ako cemo q2 izbaciti tada ce q3 postati q2
        stari_novi_index = {}
        # prolazimo kroz sva stanja i gledamo da li je dostupno
        # ako nije ne ubacujemo to stanje u listu novih stanja
        for stanje in self.stanja:
            if stanje.dostupno == True:
                nova_lista_stanja.append(stanje)
                stari_novi_index[stanje.index] = stanje.index - broj_ukljonjenih_stanja
            else:
                broj_ukljonjenih_stanja += 1

        self.stanja = nova_lista_stanja
        # promjena indexa
        i = 0

        for stanje in self.stanja:
            #npr stanje.grane = {'0':1,'1':1}
            #{'znak iz alfabeta': index stanje}
            for key in stanje.grane:
                # ako su od q0 grane {'0':3,'1':0}
                # i ako smo izbacili q2, novi q2 je postao stari q3
                # pa moramo staviti grane na {'0':2,'1':0}
                stanje.grane[key] = stari_novi_index[stanje.grane[key]]
            stanje.index = i
            i += 1

    def provjera_dostupnosti(self,stanje,pregledana_stanja):
        stanje.dostupno = True
        pregledana_stanja.append(stanje.index)
        #if dubina_rekurzije <= max_dubina:
        for key in stanje.grane:
            if stanje.grane[key] not in pregledana_stanja:
                self.provjera_dostupnosti(self.stanja[stanje.grane[key]],pregledana_stanja)

    def prihvata(self, s):
        znakovi = list(s)

        for znak in znakovi:
            if znak not in ALFABET_DFA:
                return False

        trenutno_stanje = self.pocetno_stanje
        for znak in znakovi:
            # {'znak alfabeta': index stanja}
            # npr grane = {'0':1,'1':1}
            novo_stanje_index = trenutno_stanje.grane[znak]
            trenutno_stanje = self.stanja[novo_stanje_index]
            #print("trenutno sam u stanju q"+novo_stanje_index)

        if trenutno_stanje.zavrsno == True:
            return True
        else:
            return False

class NFA_Stanje:
    def __init__(self,index,grane,zavrsno):
        self.index = index
        self.grane = {}
        for a in ALFABET_NFA:
            self.grane[a]=grane[a]
        self.zavrsno = zavrsno

class NFA:
    def __init__(self,broj_stanja):
        self.broj_stanja = 0
        self.stanja = []
        for i in range(broj_stanja):
            self.dodaj_stanje()
        self.pocetno_stanje = self.stanja[0]

    def dodaj_stanje(self):
        grane = {}
        for a in ALFABET_NFA:
            grane[a] = None
        self.stanja.append(NFA_Stanje(self.broj_stanja,grane,False))
        self.broj_stanja+=1

    def povezi_stanja(self):
        # samo za provjeru da li je ispravno unesena vrijednost
        stanja_NFA = []
        for i in range(self.broj_stanja):
            stanja_NFA.append(i)

        for q in self.stanja:
            for a in ALFABET_NFA:
                while True:
                    stanja = input("Trenutno stanje je q"+str(q.index)+" citamo znak "+a+" i idemo u stanja (npr. 1,2,3 ili None za nigdje)")
                    if stanja == "None":
                        q.grane[a]=None
                        break
                    else:
                        stanja = stanja.split(",")
                        try:
                            stanja = [int(i) for i in stanja]
                        except:
                            print("Greska pri unosu, unesite brojeve")
                        #presjek
                        if (set(stanja) & set(stanja_NFA))==set(stanja):
                            q.grane[a]= stanja
                            break
            i = input("Da li je ovo stanje zavrsno ? 1 - Da, bilo sta drugo - Ne ")
            if i == '1':
                q.zavrsno = True

    def ispisi(self):
        print("Ispis NFA")
        print("Pocetno stanje je q"+str(self.pocetno_stanje.index))
        for s in self.stanja:
            print("Iz stanja q"+str(s.index)+" imamo")
            for a in ALFABET_NFA:
                print("granu",a,"koja ide u q"+ str(s.grane[a]) if s.grane[a]!=None else "koja ne ide nigdje")
            print("Stanje q"+str(s.index)+(" je " if s.zavrsno else " nije ")+"zavrsno")

    def pretvori_u_DFA(self):
        D = DFA(2**self.broj_stanja)

        lista_stanja = []
        #[0,1,2]
        for i in range(self.broj_stanja):
            lista_stanja.append(i)

        #partitivni skup stanja
        partitivni_skup = reduce(lambda result, x: result + [subset + [x] for subset in result],
                  lista_stanja, [[]])
        partitivni_skup = sorted(partitivni_skup, key=lambda stanje: len(stanje))
        #nakon ovoga dobijamo npr za 3 stanja
        #[[], [0], [1], [2], [0, 1], [0, 2], [1, 2], [0, 1, 2]]
        #prebacimo prazan skup na kraj
        # [[0], [1], [2], [0, 1], [0, 2], [1, 2], [0, 1, 2],[]]
        partitivni_skup.append(partitivni_skup.pop(0))
        #
        # prvo odradimo stanja koja su ista kao i kod NFA
        for i in range(self.broj_stanja):
            # npr. stanje q0 u DFA ima grane kao i stanje q0 u NFA samo izbacimo e granu
            D.stanja[i].grane = self.stanja[i].grane.copy()
            D.stanja[i].grane.pop('e')
            if(self.stanja[i].zavrsno)==True:
                D.stanja[i].zavrsno = True
            # sada provjeravamo jel neka grana ide u None ili u vise stanja
            # D.stanja[i].grane = {'0':None,'1':[0,1]}
            trenutne_grane = D.stanja[i].grane
            for kljuc in trenutne_grane:
                if trenutne_grane[kljuc] == None:
                    #unmjesto None stavimo index posljednjeg stanja
                    #Za NFA sa 3 stanja imamo da grane {'0':None,'1':[0,1]}
                    #prelaze u oblik kod DFA {'0':[7],'1':[0,1]}
                    # za sada ovako ostavljamo zbog kasnijeg sabiranja grana
                    # umjesto [7] za sada postavljamo samo []
                    trenutne_grane[kljuc] = []
                    ###trenutna_grana[kljuc] = [2**self.broj_stanja-1]
                else:
                    # imamo da su grane od q0 {'0':[7],'1':[0,1]} i sada je recimo stanja=[0,1]
                    # treba za svaki index iz stanja (0 i 1) provjeriti da li ima e grana
                    # i ako ima treba dodati odgovarajuca stanja u koje ide e
                    konacna_stanja = trenutne_grane[kljuc].copy()
                    #stanja = [0,1]
                    #index_stanja = 0 recimo
                    #provjeriti e grane za stanja u koje dolazimo putem
                    # q0 ---e--> [qi,qj,..]
                    for index_stanja in trenutne_grane[kljuc]:
                        konacna_stanja+=self.provjeri_e(self.stanja[index_stanja].grane['e'])
                    #uklonimo dublikate i sortiramo
                    konacna_stanja = list(set(konacna_stanja))
                    # ako je konacna stanja [0,1]
                    # onda trazimo index od [0,1] u parititvnom skupu
                    # [[0], [1], [2], [0, 1], [0, 2], [1, 2], [0, 1, 2],[]]
                    # a to je 3
                    # pa sada umjesto
                    # D.stanja[i].grane = {'0': [], '1': [0, 1]}
                    # imamo da je
                    # D.stanja[i].grane = {'0':[],'1':[3]} i to cemo kasnije uraditi jer
                    # zbog sabiranja za sada ostavljamo
                    trenutne_grane[kljuc] = konacna_stanja
                    ###trenutne_grane[kljuc] = [partitivni_skup.index(konacna_stanja)]
        #ostala stanja dobijamo "sabiranjem" ovih "osnovnih" stanja
        for i in range(self.broj_stanja,2**self.broj_stanja-1):
            # [[0], [1], [2], [0, 1], [0, 2], [1, 2], [0, 1, 2],[]]
            # trenutno stanje DFA je recimo [0,1] a index mu je i=3
            trenutno_stanje_DFA = partitivni_skup[i]
            # grane ovog stanja su jednake "sumi" grana stanja q0 i q1 koje smo vec napravili
            # grana od q0
            suma_grana = D.stanja[trenutno_stanje_DFA[0]].grane.copy()

            #provjera za zavrsno
            if D.stanja[trenutno_stanje_DFA[0]].zavrsno == True:
                D.stanja[i].zavrsno = True

            for j in trenutno_stanje_DFA[1:]:
                #provjera za zavrsno
                if D.stanja[j].zavrsno == True:
                    D.stanja[i].zavrsno = True

                grana_1 = D.stanja[j].grane
                #saberi
                suma_grana = {k: suma_grana.get(k, 0) + grana_1.get(k, 0) for k in set(suma_grana)}
            for kljuc in suma_grana:
                #uklonimo duplikate
                suma_grana[kljuc] = list(set(suma_grana[kljuc]))
            D.stanja[i].grane = suma_grana

        # Kako sada imamo stanja u DFA koja idu u vise stanja recimo u  [0,1] ili [1,2] ili druga
        # sada iz partitivnog skupa [[0], [1], [2], [0, 1], [0, 2], [1, 2], [0, 1, 2],[]]
        # kojem odgovara poredak novih stanja u DFA, moramo ih konvertovati u stvarne indexe,
        # recimo stanju [0,1] odgovara index 3, stanju [1,2] odgovara index 5 (q5)

        for stanje in D.stanja:
            # ako stanje odgovara praznom skupu
            if stanje.index == len(partitivni_skup) - 1:
                break
            for kljuc in stanje.grane:
                oznaka_stanja = stanje.grane[kljuc]
                pravi_index = partitivni_skup.index(oznaka_stanja)
                stanje.grane[kljuc] = pravi_index
        #postavimo da grane iz stanja "praznog skupa" idu u samog sebe
        for kljuc in D.stanja[len(partitivni_skup)-1].grane:
            D.stanja[len(partitivni_skup) - 1].grane[kljuc]=len(partitivni_skup)-1
        # jer je uvijek u NFA q0 pocetno
        pocetna_stanja = self.provjeri_e([0])
        index_pocetnog_u_DFA = partitivni_skup.index(pocetna_stanja)
        D.pocetno_stanje = D.stanja[index_pocetnog_u_DFA]

        return D

    def provjeri_e(self,stanja):
        if stanja == None:
            return []
        #ako e grana ide u jedno stanje
        elif len(stanja)==1:
            #imamo npr samo stanja = [1]
            index_novog_stanja = stanja[0]
            return stanja+self.provjeri_e(self.stanja[index_novog_stanja].grane['e'])
        #ako e ide u vise od jednog stanja recimo [0,1,2]
        # return provjeri_e([0])+provjeri_e([1,2])
        else:
            return self.provjeri_e(stanja[:1])+self.provjeri_e(stanja[1:])





#D=DFA(2)
#D.povezi_stanja()
#D.ispisi()
#print(D.prihvata("1211"))
#print(D.prihvata("111"))
N=NFA(3)
N.povezi_stanja()
N.ispisi()
print("######")
D = N.pretvori_u_DFA()
D.ispisi()
D.obrisi_nepotrebna_stanja()
D.ispisi()
print(D.prihvata("11010000"))
print(D.prihvata("11111"))
